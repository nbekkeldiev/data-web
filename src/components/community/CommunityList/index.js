import React, { Component } from 'react';

import EntityList from 'components/common/EntityList';

class CommunityList extends Component {
  render() {
    return (
      <EntityList
        entity="community"
        endpoint="communities"
        fields={['population', 'households', 'wsp']}
      />
    );
  }
}

export default CommunityList;
