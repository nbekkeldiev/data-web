import React, { Component } from 'react';
import { entityColors } from 'lib/Utils';
import Entities from 'components/common/Entities';

class Communities extends Component {
  render() {
    return (
      <Entities
        entity="community"
        endpoint="communities"
        indicator="wsp"
        stats
        backgroundColor={entityColors[0]}
      />
    );
  }
}


export default Communities;
