import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { calculateScore } from 'lib/Utils';

import ScoreTree from 'components/common/ScoreTree';

class CommunityScoreTree extends Component {
  render() {
    const { t } = this.props;
    return (
      <ScoreTree>
        <path d="M255,55 L117.5,180 Z" className="score-tree-line" />
        <path d="M255,55 L392.5,180 Z" className="score-tree-line" />
        <path d="M115.5,180 L55,305 Z" className="score-tree-line" />
        <path d="M115.5,180 L180,305 Z" className="score-tree-line" />
        <path d="M392.5,180 L330,305 Z" className="score-tree-line" />
        <path d="M392.5,180 L455,305 Z" className="score-tree-line" />
        <g id="wsp" transform="translate(255, 55)">
          <circle r="65" className={`score-tree-circle background-${calculateScore(this.props.wsp)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsp)}`}>
            {t('wsp')}
          </text>
        </g>
        <g id="wshl" transform="translate(117.5, 180)">
          <circle r="50" className={`score-tree-circle background-${calculateScore(this.props.wshl)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wshl)}`}>
            {t('wshl')}
          </text>
        </g>
        <g id="wssi" transform="translate(392.5, 180)">
          <circle r="50" className={`score-tree-circle background-${calculateScore(this.props.wssi)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wssi)}`}>
            {t('wssi')}
          </text>
        </g>
        <g id="wsl" transform="translate(55, 305)">
          <circle r="35" className={`score-tree-circle background-${calculateScore(this.props.wsl)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsl)}`}>
            {t('wsl')}
          </text>
        </g>
        <g id="shl" transform="translate(180, 305)">
          <circle r="35" className={`score-tree-circle background-${calculateScore(this.props.shl)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.shl)}`}>
            {t('shl')}
          </text>
        </g>
        <g id="wsi" transform="translate(330, 305)">
          <circle r="35" className={`score-tree-circle background-${calculateScore(this.props.wsi)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.wsi)}`}>
            {t('wsi')}
          </text>
        </g>
        <g id="sep" transform="translate(455, 305)">
          <circle r="35" className={`score-tree-circle background-${calculateScore(this.props.sep)}`} />
          <text y="7.5" className={`score-tree-text score-tree-text-${calculateScore(this.props.sep)}`}>
            {t('sep')}
          </text>
        </g>
      </ScoreTree>
    );
  }
}

CommunityScoreTree.propTypes = {
  wsp: PropTypes.number,
  wshl: PropTypes.number,
  wssi: PropTypes.number,
  wsl: PropTypes.number,
  shl: PropTypes.number,
  wsi: PropTypes.number,
  sep: PropTypes.number,
  t: PropTypes.func.isRequired,
};

CommunityScoreTree.defaultProps = {
  wsp: null,
  wshl: null,
  wssi: null,
  wsl: null,
  shl: null,
  wsi: null,
  sep: null,
};

export default translate()(CommunityScoreTree);
