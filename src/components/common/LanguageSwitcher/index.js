import React, { Component } from 'react';
import { ButtonDropdown, DropdownMenu, DropdownItem, DropdownToggle } from 'reactstrap';
import PropTypes from 'prop-types';
import { translate } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGlobe } from '@fortawesome/free-solid-svg-icons';

class LanguageSwitcher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false,
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }


  render() {
    const { t, i18n } = this.props;

    const changeLanguage = (lng) => {
      i18n.changeLanguage(lng);
    };

    return (
      <ButtonDropdown nav isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle nav caret className="pl-0">
          <FontAwesomeIcon icon={faGlobe} fixedWidth />
          <span className="d-none d-md-inline pl-2">{t('language')}</span>
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={() => changeLanguage('es-ES')}>ES</DropdownItem>
          <DropdownItem onClick={() => changeLanguage('en-EN')}>EN</DropdownItem>
          <DropdownItem onClick={() => changeLanguage('pt-BR')}>PT</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
    );
  }
}

LanguageSwitcher.propTypes = {
  t: PropTypes.func.isRequired,
  i18n: PropTypes.object.isRequired,
};

export default translate()(LanguageSwitcher);
