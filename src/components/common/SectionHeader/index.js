import React, { Component } from 'react';
import PropTypes from 'prop-types';

import './style.css';

class SectionHeader extends Component {
  render() {
    return (
      <div className={`section-header p-3 bg-primary text-secondary ${this.props.className}`}>
        { this.props.title ? (
          <h5 className="mb-0">{this.props.title}</h5>
        ) : this.props.children }
      </div>
    );
  }
}

SectionHeader.propTypes = {
  title: PropTypes.string,
  children: PropTypes.arrayOf(PropTypes.element),
  className: PropTypes.string,
};

SectionHeader.defaultProps = {
  title: null,
  children: null,
  className: null,
};

export default SectionHeader;
