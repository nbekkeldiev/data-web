import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, CardTitle, CardSubtitle } from 'reactstrap';

import Loading from 'components/common/Loading';

import './style.css';

class Chart extends Component {
  render() {
    return (
      <Card color={this.props.color} className="mb-4 chart-card">
        <CardBody className="p-0">
          <CardTitle>{this.props.title}</CardTitle>
          <CardSubtitle>{this.props.subtitle}</CardSubtitle>
          <div className="mt-3">
            {this.props.data ? (
              this.props.children
            ) : (
              <Loading />
            )}
          </div>
        </CardBody>
      </Card>
    );
  }
}

Chart.propTypes = {
  color: PropTypes.string,
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  children: PropTypes.element.isRequired,
  data: PropTypes.object,
};

Chart.defaultProps = {
  color: 'default',
  subtitle: null,
  data: null,
};


export default Chart;
