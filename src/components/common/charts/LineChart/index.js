import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Line } from 'react-chartjs-2';

import Chart from 'components/common/charts/Chart';

const processData = (data, colors) => {
  const datasets = data.datasets.map((dataset, index) => Object.assign({}, dataset, {
    borderColor: colors ? colors[index] : null,
    backgroundColor: 'transparent',
  }));

  return {
    ...data,
    datasets,
  };
};

class LineChart extends Component {
  state = {
    data: null,
  }

  componentDidUpdate(prevProps) {
    if (!this.props.data || !this.props.data.datasets || !this.props.colors) return;
    if (prevProps.data !== this.props.data) {
      this.setState({
        data: processData(this.props.data, this.props.colors),
      });
    }
  }

  render() {
    return (
      <Chart title={this.props.title} subtitle={this.props.subtitle} data={this.state.data}>
        <Line
          data={this.state.data}
          options={this.props.options}
          legend={{
            display: true,
            position: 'bottom',
            fullWidth: true,
            ...this.props.legend,
          }}
        />
      </Chart>
    );
  }
}

LineChart.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.object,
  options: PropTypes.object,
  subtitle: PropTypes.string,
  colors: PropTypes.arrayOf(PropTypes.string),
  legend: PropTypes.object,
};

LineChart.defaultProps = {
  options: {},
  subtitle: null,
  colors: [],
  data: null,
  legend: {},
};

export default LineChart;
