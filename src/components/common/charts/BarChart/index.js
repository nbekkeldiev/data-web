import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Bar } from 'react-chartjs-2';

import Chart from 'components/common/charts/Chart';

const processData = (data, colors) => {
  const datasets = data.datasets.map((dataset, index) => Object.assign({}, {
    backgroundColor: colors ? colors[index] : null,
    hoverBackgroundColor: colors ? colors[index] : null,
    borderWidth: 0,
  }, dataset));

  return {
    ...data,
    datasets,
  };
};

class BarChart extends Component {
  state = {
    data: null,
  }

  componentDidUpdate(prevProps) {
    if (!this.props.data || !this.props.data.datasets || !this.props.colors) return;
    if (prevProps.data !== this.props.data) {
      this.setState({
        data: processData(this.props.data, this.props.colors),
      });
    }
  }

  render() {
    return (
      <Chart title={this.props.title} subtitle={this.props.subtitle} data={this.state.data}>
        <Bar
          data={this.state.data}
          options={this.props.options}
          legend={{
            display: true,
            position: 'bottom',
            fullWidth: true,
            ...this.props.legend,
          }}
          height={this.props.height}
        />
      </Chart>
    );
  }
}

BarChart.propTypes = {
  title: PropTypes.string.isRequired,
  data: PropTypes.object,
  options: PropTypes.object,
  subtitle: PropTypes.string,
  colors: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.string),
    PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.string)),
  ]),
  legend: PropTypes.object,
  height: PropTypes.number,
};

BarChart.defaultProps = {
  options: {},
  subtitle: null,
  colors: null,
  data: null,
  legend: {},
  height: null,
};

export default BarChart;
